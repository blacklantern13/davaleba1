package com.example.davaleba_1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.davaleba_1.fragments.ContactFragment
import com.example.davaleba_1.fragments.DashboardFragment
import com.example.davaleba_1.fragments.InfoFragment
import com.example.davaleba_1.fragments.LiveFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    private val dashboardFragment = DashboardFragment()
    private val infoFragment = InfoFragment()
    private val liveFragment = LiveFragment()
    private val contactFragment = ContactFragment()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        replaceFragment(dashboardFragment)

        var botNav: BottomNavigationView = findViewById(R.id.bot_nav)
        botNav.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.dashboard -> replaceFragment(dashboardFragment)
                R.id.info -> replaceFragment(infoFragment)
                R.id.liveHelp -> replaceFragment(liveFragment)
                R.id.contact -> replaceFragment(contactFragment)
            }
            true
        }

    }

    private fun replaceFragment(fragment: Fragment) {
        if (fragment != null) {
            val replace = supportFragmentManager.beginTransaction()
            replace.replace(R.id.fragment_container, fragment)
            replace.commit()
        }
    }
}